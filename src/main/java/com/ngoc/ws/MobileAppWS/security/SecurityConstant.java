package com.ngoc.ws.MobileAppWS.security;

import com.ngoc.ws.MobileAppWS.MobileAppWsApplication;
import com.ngoc.ws.MobileAppWS.SpringApplicationContext;

public class SecurityConstant {

    public static final int EXPIRATION_TIME = 8400000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users";

    public static String getTokenSecret(){

        AppProperties appProperties = (AppProperties) SpringApplicationContext.getBean("appProperties");

        return appProperties.getTokenSecret();
    }
}
