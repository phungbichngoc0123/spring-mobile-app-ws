package com.ngoc.ws.MobileAppWS.service.impl;

import com.ngoc.ws.MobileAppWS.exception.UserServiceException;
import com.ngoc.ws.MobileAppWS.io.entity.UserEntity;
import com.ngoc.ws.MobileAppWS.io.repository.UserRepository;
import com.ngoc.ws.MobileAppWS.service.UserService;
import com.ngoc.ws.MobileAppWS.shared.Utils;
import com.ngoc.ws.MobileAppWS.shared.dto.AddressDto;
import com.ngoc.ws.MobileAppWS.shared.dto.UserDto;
import com.ngoc.ws.MobileAppWS.ui.response.ErrorMessages;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.ModelMap;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private Utils utils;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDto createUser(UserDto userDto) {

        //check email exist
        if(userRepository.findByEmail(userDto.getEmail()) != null) {
            throw new RuntimeException("Record already exists");
        }

        //set address in userdto
        for(int i = 0; i < userDto.getAddresses().size(); i++){
            AddressDto addressDto = userDto.getAddresses().get(i);
            addressDto.setUserDetails(userDto);
            addressDto.setAddressId(utils.generateAddressId(30));
            userDto.getAddresses().set(i, addressDto);
        }

        //save userDto to db via userEntity
//        UserEntity userEntity = new UserEntity();
//        BeanUtils.copyProperties(userDto, userEntity);
        ModelMapper mapper = new ModelMapper();
        UserEntity userEntity = mapper.map(userDto, UserEntity.class);

        String generatedUserId = utils.generateUserId(30);
        userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        userEntity.setUserId(generatedUserId);

        UserEntity savedUserDetail = userRepository.save(userEntity);

        //return value
//        UserDto returnValue = new UserDto();
//        BeanUtils.copyProperties(savedUserDetail,returnValue);
        UserDto returnValue = mapper.map(savedUserDetail, UserDto.class);

        return returnValue;
    }

    @Override
    public UserDto getUser(String email) {
        UserDto userDto = new UserDto();

        //Find by email from user repository
        UserEntity entity = userRepository.findByEmail(email);

        //check null
        if(entity == null){
            throw new UsernameNotFoundException(email);
        }

        //copy user entity to userDto
        BeanUtils.copyProperties(entity, userDto);

        //return user dto
        return userDto;
    }

    @Override
    public UserDto getUserById(String id) {
        //init a userDto to return
        UserDto returnValue = new UserDto();

        //get user info from db
        UserEntity entity = userRepository.findByUserId(id);

        //check null
        if(entity == null){
            throw new UsernameNotFoundException(id);
        }

        //copy data from db to target object
        BeanUtils.copyProperties(entity, returnValue);

    //return userdto
        return returnValue;
    }

    @Transactional
    @Override
    public UserDto updateUser(String userId, UserDto userDto) {
        UserDto returnValue = new UserDto();

        //find user entity by id
        UserEntity entity = userRepository.findByUserId(userId);

        //check null
        if(entity == null){
            throw new UsernameNotFoundException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
        }

        //update data
        entity.setFirstName(userDto.getFirstName());
        entity.setLastName(userDto.getLastName());

        UserEntity updatedUserDetail = userRepository.save(entity);

        //copy to dto
        BeanUtils.copyProperties(updatedUserDetail, returnValue);

        //return
        return returnValue;
    }

    @Transactional
    @Override
    public void deleteUser(String userId) {
        UserEntity entity = userRepository.findByUserId(userId);

        if(entity == null){
            throw new UsernameNotFoundException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
        }

        userRepository.delete(entity);
    }

    @Override
    public List<UserDto> getUsers(int page, int limit) {
        List<UserDto> returnValue = new ArrayList<>();

        Pageable pageRequest = PageRequest.of(page, limit);
        Page<UserEntity> userEntityPage =  userRepository.findAll(pageRequest);

        List<UserEntity> users = userEntityPage.getContent();

        //copy to userDto
        for(UserEntity entity : users){
            UserDto userDto = new UserDto();
            BeanUtils.copyProperties(entity, userDto);
            returnValue.add(userDto);
        }

        return returnValue;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByEmail(email);

        //check null
        if(userEntity == null) {
            throw new UsernameNotFoundException(email + " not found");
        }
        return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(), new ArrayList<>());
    }
}
