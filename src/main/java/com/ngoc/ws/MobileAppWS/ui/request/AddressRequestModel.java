package com.ngoc.ws.MobileAppWS.ui.request;

public class AddressRequestModel {

    private String city;

    private String country;

    private String streetName;

    private String postalCode;

    private String type;

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setType(String type) {
        this.type = type;
    }
}
