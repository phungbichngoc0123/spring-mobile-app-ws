package com.ngoc.ws.MobileAppWS.ui.controller;

import com.ngoc.ws.MobileAppWS.exception.UserServiceException;
import com.ngoc.ws.MobileAppWS.service.UserService;
import com.ngoc.ws.MobileAppWS.shared.dto.UserDto;
import com.ngoc.ws.MobileAppWS.ui.response.*;
import com.ngoc.ws.MobileAppWS.ui.request.UserDetailRequestModel;
import com.sun.org.apache.bcel.internal.generic.RETURN;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_JSON_VALUE})
    public UserRest getUser(@PathVariable String id){

        //declear a user info return in response
        UserRest returnValue = new UserRest();

        //get from userService
        UserDto userDto = userService.getUserById(id);

        //check null
        if (userDto == null){
            throw new UsernameNotFoundException("User not found id-" + id);
        }

        // copy to userRest
        BeanUtils.copyProperties(userDto, returnValue);

        //return userRest
        return returnValue;
    }

    @PostMapping( consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public UserRest createUser(@RequestBody UserDetailRequestModel userDetails) throws Exception{

        //create response object
        UserRest returnValue = new UserRest();

        //check require field from request data
        if(userDetails.getFirstName().isEmpty()){
            throw new UserServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
//            throw new NullPointerException("Object is null");
        }

        //create user dto object and receive the information from response
//        UserDto userDto = new UserDto();
//        BeanUtils.copyProperties(userDetails, userDto);
        ModelMapper modelMapper = new ModelMapper();
        UserDto userDto = modelMapper.map(userDetails, UserDto.class);

        //service layer handle business logic and return an user dto object(prepared object)
        UserDto createdUser = userService.createUser(userDto);
        //map data from service to response object
        returnValue = modelMapper.map(createdUser, UserRest.class);

        //return response object
        return returnValue;
    }

    @PutMapping("/{id}")
    public UserRest updateUesr(@PathVariable String id, @RequestBody UserDetailRequestModel userDetails){
        //create response object
        UserRest returnValue = new UserRest();

        //check require field from request data
        if(userDetails.getFirstName().isEmpty()){
            throw new UserServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
//            throw new NullPointerException("Object is null");
        }

        //create user dto object and receive the information from response
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(userDetails, userDto);

        //service layer handle business logic and return an user dto object(prepared object)
        UserDto updatedUser = userService.updateUser(id, userDto);
        //map data from service to response object
        BeanUtils.copyProperties(updatedUser, returnValue);

        //return response object
        return returnValue;
    }

    @DeleteMapping("/{id}")
    public OperationStatusModel deleteUser(@PathVariable String id){

        OperationStatusModel returnValue = new OperationStatusModel();
        returnValue.setOperationName(RequestOperationName.DELETE.name());
        returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());

        //perform delete
        userService.deleteUser(id);

        return returnValue;
    }

    @GetMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public List<UserRest> getUsers(@RequestParam(value="page", defaultValue = "0") int page,
                                   @RequestParam(value="limit", defaultValue = "10") int limit){
        List<UserRest> returnValue = new ArrayList<>();

        if(page > 0){
             page = page -1;
        }

        //call service to get list of users
        List<UserDto> users = userService.getUsers(page, limit);

        //copy to return value
        for(UserDto user : users){
            UserRest tempUserRest = new UserRest();
            BeanUtils.copyProperties(user, tempUserRest);
            returnValue.add(tempUserRest);
        }

        //return value
        return returnValue;
    }
}
