package com.ngoc.ws.MobileAppWS.exception;

public class UserServiceException extends RuntimeException{
    private static final long serialVersionUID = -1389433856778653696L;

    public UserServiceException(String message) {
        super(message);
    }
}
