package com.ngoc.ws.MobileAppWS.exception;

import com.ngoc.ws.MobileAppWS.ui.response.ErrorMessage;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class AppExceptionHandler {

    @ExceptionHandler(value = {UserServiceException.class})
    public ResponseEntity<Object> handlerUerServiceException(UserServiceException exc, WebRequest request){

        //set data for error message
        ErrorMessage errorMessage = new ErrorMessage(new Date(), exc.getMessage());

        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handlerOtherException(UserServiceException exc, WebRequest request){

        //set data for error message
        ErrorMessage errorMessage = new ErrorMessage(new Date(), exc.getMessage());

        return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
